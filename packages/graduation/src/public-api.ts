/*
 * Public API Surface of graduation
 */

export * from './lib/graduation.service';
export * from './lib/graduation.component';
export * from './lib/graduation.module';
