import { NgModule } from '@angular/core';
import { GraduationComponent } from './graduation.component';



@NgModule({
  declarations: [GraduationComponent],
  imports: [
  ],
  exports: [GraduationComponent]
})
export class GraduationModule { }
